package com.company.testbarcode.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.cuba.core.global.DesignSupport;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;

@DesignSupport("{'imported':true}")
@Table(name = "WorkLog")
@Entity(name = "testbarcode$WorkLog")
public class WorkLog extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = 7904559831879118763L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "\"IDOrder\"")
    protected Orders iDOrder;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "\"Barcode\"")
    protected Orders barcode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "\"Start\"", nullable = false)
    protected Date start;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "\"End\"")
    protected Date end;

    public void setIDOrder(Orders iDOrder) {
        this.iDOrder = iDOrder;
    }

    public Orders getIDOrder() {
        return iDOrder;
    }

    public void setBarcode(Orders barcode) {
        this.barcode = barcode;
    }

    public Orders getBarcode() {
        return barcode;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStart() {
        return start;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getEnd() {
        return end;
    }


}