package com.company.testbarcode.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.global.DesignSupport;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.BaseIntIdentityIdEntity;

@DesignSupport("{'imported':true}")
@NamePattern("%s|description")
@Table(name = "Orders")
@Entity(name = "testbarcode$Orders")
public class Orders extends BaseIntIdentityIdEntity {
    private static final long serialVersionUID = 7588752344938758463L;

    @Column(name = "\"Number\"", length = 50)
    protected String number;

    @Column(name = "\"Description\"", length = 50)
    protected String description;

    @Column(name = "\"Barcode\"", length = 5)
    protected String barcode;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBarcode() {
        return barcode;
    }


}